import dash
from dash import Dash, dcc, html, Input, Output, callback
import plotly
from acceso import accesos

ref = accesos()

app = Dash(__name__)

app.layout = html.Div(
    html.Div([
        html.H4('Gráfica en tiempo real'),
        html.Div(id='live-update-text'),
        dcc.Graph(id='live-update-graph'),
        dcc.Interval(
            id='interval-component',
            interval=1*1000,
            n_intervals=0
        )
    ])
) 

@callback(
    Output('live-update-text', 'children'),
    Input('interval-component', 'n_intervals')
)
 
def update_metrics(n):
    x = ref.get()['x']
    y = ref.get()['y']
    return [
        html.Span('x: {}'.format(x[-1])),
        html.Span(' '),
        html.Span('y: {}'.format(y[-1])),
    ]

@callback(
    Output('live-update-graph', 'figure'),
    Input('interval-component', 'n_intervals'),
)

def update_graph_live(n):
    x = ref.get()['x']
    y = ref.get()['y']
    data = plotly.graph_objs.Scatter(
        x=x,
        y=y,
        name='Scatter',
        mode='lines+markers'
    )
    return {'data': [data], 'layout': plotly.graph_objs.Layout(xaxis=dict(range=[min(x), max(x)]), yaxis=dict(range=[min(y), max(y)]),)}

if __name__ == '__main__':
    app.run(debug=True)



